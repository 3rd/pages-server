package cache

import "time"

type SetGetKey interface {
	Set(key string, value interface{}, ttl time.Duration) error
	Get(key string) (interface{}, bool)
	Remove(key string)
}
